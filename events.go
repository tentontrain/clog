// Various log events.

package clog

// Generic log event
type Entry string

func (e Entry) String() string {
	return string(e)
}

// Info string
type Info string

func (l Info) String() string {
	return "info: " + string(l)
}

// Error
type Error string

func (e Error) String() string {
	return "error: " + string(e)
}

// Critical error
type Fatal string

func (c Fatal) String() string {
	return "fatal: " + string(c)
}
