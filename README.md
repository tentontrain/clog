Clog is a concurrent log. It stores log entries in a buffer and writes (flushes) them out when the
log buffer gets full. Log entries that are in memory (not flushed) can be searched and counted.
Multiple goroutines can log concurrently without blocking, even if there is an ongoing flush or
search.

Example usage:

```go
package main

import (
	"bitbucket.org/tentontrain/clog"
	"fmt"
	"os"
	"time"
)

var log clog.Log

func init() {
	file, _ := os.OpenFile("log.txt", os.O_RDWR|os.O_APPEND|os.O_CREATE, 0666)
	log.Initialize(1024, file, 32)
}

func main() {
	// log stuff concurrently
	for i := 0; i < 10; i++ {
		go func(a int) {
			log.Log(a)
		}(i)
	}

	// search and print contents of log (does not block writters - their writes are buffered)
	// note: buffered entries are not searched
	go fmt.Println(
		log.Search(func(a fmt.Stringer) bool {
			return true // get everything
		}))

	time.Sleep(time.Millisecond * 1)

	// stop log and write everything (log contents and buffered entries) to file
	log.Stop()
}

```

[![GoDoc](https://godoc.org/bitbucket.org/tentontrain/clog?status.svg)](https://godoc.org/bitbucket.org/tentontrain/clog)
