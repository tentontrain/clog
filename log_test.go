package clog

import (
	"bytes"
	"fmt"
	"testing"
	"time"
)

type Event int

func (e Event) String() string {
	return string(e)
}

func TestLog(t *testing.T) {

	ll := Log{}
	var buf bytes.Buffer
	ll.Initialize(100, &buf, 10)

	for n := 0; n < 3; n++ {
		go func() {
			time.Sleep(5 * time.Millisecond)
			for i := 0; i < 10; i++ {
				ll.Log(Event(i))
			}
		}()
	}

	time.Sleep(200 * time.Millisecond)

	for i := 0; i < 10; i++ {
		c := ll.Count(
			func(s fmt.Stringer) bool {
				t, ok := s.(Event)
				if ok && int(t) == i {
					return true
				}
				return false
			})

		if c != 3 {
			t.Error(" count is ", c)
		}
	}

	ll.Flush()

	for i := 0; i < 10; i++ {
		c := ll.Count(
			func(s fmt.Stringer) bool {
				t, ok := s.(Event)
				if ok && int(t) == i {
					return true
				}
				return false
			})
		if c != 0 {
			t.Error(" count is ", c)
		}
	}
}

func TestLogFlush(t *testing.T) {
	ll := Log{}
	var buf bytes.Buffer
	ll.Initialize(10, &buf, 1)

	for i := 0; i < 11; i++ {
		ll.Log(Event(i))
	}

	time.Sleep(10 * time.Millisecond)

	// should have flushed
	if len(ll.events) != 1 {
		t.Error("did not flush ", len(ll.events))
	}

	ll.Log(Event(11))
	ll.Stop()

	if len(ll.events) != 0 {
		t.Error("did not flush ", len(ll.events))
	}
}

func TestInfo(t *testing.T) {
	ll := Log{}
	var buf bytes.Buffer
	ll.Initialize(1, &buf, 100)

	ll.Info("1", 2, 2, "3")
	ll.Stop()

	if buf.String() != "info: 12 23\n" {
		t.Error(buf.String())
	}
}

func TestSearch(t *testing.T) {
	ll := Log{}
	ll.Initialize(10, nil, 2)

	for i := 0; i < 10; i += 2 {
		ll.Info(i)
	}

	for i := 1; i < 10; i += 2 {
		ll.Log(Error("oh no"))
	}

	ll.Stop() //make sure nothing is in the buffer

	s := ll.Search(func(a fmt.Stringer) bool {
		if _, ok := a.(Info); ok {
			return true
		}
		return false
	})

	if len(s) != 5 {
		t.Error(len(s))
	}

	if s[4].String() != "info: 8" {
		t.Error(s[4])
	}

	s = ll.Search(func(a fmt.Stringer) bool {
		if _, ok := a.(Error); ok {
			return true
		}
		return false
	})

	if len(s) != 5 {
		t.Error(len(s))
	}

	if s[4].String() != "error: oh no" {
		t.Error(s[4])
	}

}
