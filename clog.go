// Package clog is a concurrent log. It stores log entries in a buffer and writes (flushes) them
// out when the log buffer is full. Log entries that are in memory (not flushed) can be searched and
// counted. Multiple goroutines can log concurrently without blocking, even if there is an ongoing
// flush or search.
package clog

import (
	"fmt"
	"io"
	"os"
	"sync"
)

// Log is the main log type. Must be initialized before use.
type Log struct {
	events []fmt.Stringer    // Main log buffer
	out    io.Writer         // Flush log here
	inc    chan fmt.Stringer // Used to buffer writes
	done   chan bool         // Signals that adder is finished
	lock   sync.RWMutex      // Used to block writes when flushing/counting
}

// Initialize a new logger. Parameter size determines the number of events to hold before flushing
// to out. Parameter out is the writer used to store log entries on a flush (can be nil). Parameter
// bufsize is the size of the temporary buffer used to avoid stalls. If set to zero log call block.
func (l *Log) Initialize(size int, out io.Writer, bufsize int) {
	l.events = make([]fmt.Stringer, 0, size)
	l.out = out
	l.inc = make(chan fmt.Stringer, bufsize)
	l.done = make(chan bool)
	go l.adder()
}

// Log adds any fmt.Stringer to the log.
func (l *Log) Log(s fmt.Stringer) {
	l.inc <- s
}

func (l *Log) LogPrint(a ...interface{}) {
	e := Entry(fmt.Sprint(a...))
	l.inc <- e
}

// Error logs an error. It will be prefixed with 'error:'.
func (l *Log) Error(a ...interface{}) {
	e := Error(fmt.Sprint(a...))
	l.inc <- e
}

// Info prefixes log entry with 'info:'.
func (l *Log) Info(a ...interface{}) {
	info := Info(fmt.Sprint(a...))
	l.inc <- info
}

// Fatal logs the error then exits the program.
func (l *Log) Fatal(a ...interface{}) {
	f := Fatal(fmt.Sprint(a...))
	l.inc <- f
	l.Stop()
	os.Exit(1)
}

// Stop the log. Both the log contents and the tempory buffer are flushed.
// To log again after Stop, call Initialize. Count and Search can be used after Stop.
func (l *Log) Stop() {
	close(l.inc)
	<-l.done // wait for inc to empty
	l.Flush()
}

// Store event
func (l *Log) add(e fmt.Stringer) {
	l.lock.Lock()
	defer l.lock.Unlock()

	if len(l.events) == cap(l.events) {
		l.flush()
	}
	l.events = append(l.events, e)
}

// Store events coming on inc.
func (l *Log) adder() {
	for v := range l.inc {
		l.add(v)
	}
	l.done <- true
}

// Flush log contents. Will not flush contents of temporary buffer (use Stop for that).
func (l *Log) Flush() {
	l.lock.Lock()
	defer l.lock.Unlock()
	l.flush()
}

func (l *Log) flush() {
	// no writer specified
	if l.out == nil {
		return
	}

	buf := []byte{}
	for i := 0; i < len(l.events); i++ {
		eventStr := l.events[i].String()
		buf = append(buf, eventStr...)
		buf = append(buf, '\n')
	}

	l.out.Write(buf)
	l.events = l.events[:0]
}

// Count events that satisfy the match function.
func (l *Log) Count(match func(fmt.Stringer) bool) int {
	l.lock.RLock()
	defer l.lock.RUnlock()

	c := 0
	for i := 0; i < len(l.events); i++ {
		if match(l.events[i]) {
			c++
		}
	}
	return c
}

// Search returns all events that satisfy match.
func (l *Log) Search(match func(fmt.Stringer) bool) []fmt.Stringer {
	l.lock.RLock()
	defer l.lock.RUnlock()

	var s []fmt.Stringer
	for i := 0; i < len(l.events); i++ {
		if match(l.events[i]) {
			s = append(s, l.events[i])
		}
	}
	return s
}
